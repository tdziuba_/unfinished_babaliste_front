# Installation and building

Frontend based on Bootstrap v.4, jQuery, Javascript with EcmaScript 2015 (ES6) 
and Sass (SCSS). The whole source code for frontend is located in `/front-src` folder, where config files
are located directly in front-src folder and the source files in front folder.

For building it use webpack 2 with Babel. To build and run you will need:

1. Nodejs - https://nodejs.org/en/download/ with npm package manager installed
2. globaly installed webpack and babel:

`npm install -g babel babel-cli webpack`
3. Other dependencies you'll install by running command

`npm install`
4. After installation for build the project, you'll need to run command:

`npm run build`

All built files are generated into `web` folder

## Development

For developing purposes you need to run command:
`npm run dev`

It will build developer version (non minified files, quicker build process) and watch changes.
Change in source files will generate new files into web folder.

### Templates

We use Twig for typical phtml templates. Documentation for Twig you'll find here: [http://twig.sensiolabs.org/doc/2.x/](http://twig.sensiolabs.org/doc/2.x/)

For templates, which are generated in js objects, we use Handlebars enclosed with script tag 

```
<script id="customHandlebarsTemplate" type="text/x-handlebars-template">
    <div class="entry">
      <h1>{{title}}</h1>
      <div class="body">
        {{body}}
      </div>
    </div>
</script>
```
Handlebars documentation is here: [http://handlebarsjs.com](http://handlebarsjs.com/)

### Icons:

We use webfont icons located in folder `web/webfonts/` with name icomoon.[extension] 
If you want to modify or inspect icons - best way is to use icomoon app in which it was generated.

Open in browser [https://icomoon.io/app/](https://icomoon.io/app/) and click Import icons button, 
then upload file selection.json from `web/webfonts` folder. You should see our icons.
when you finish don't forget to save your icon set again to selection.json file. 
Upload updated files dow webfonts folder and css content to `front-src/front/scss/lib/_icons.scss` file.

### Sass
Sass use bootstrap mixins and few own, which are located in `front-src/front/scss/lib/_mixins.scss`.
Own mixins are described with usage in `_mixins.scss` file, Bootstrap mixins documentation you'll
 need to find yourself :)
 
For media queries we use bootstrap mixins:

* for mobile only: `@include media-breakpoint-down(sm) {}`
* for tablet only: `@include media-breakpoint-only(md) {}`
* for mobile and tablet: `@include media-breakpoint-down(md) {}`
* for desktop and large screens: `@include media-breakpoint-up(lg) {}`

### JS

We use few libraries here:
* jQuery - as a base for DOM manipulations
* Hammer.js for swipe and touch events
* Bootstrap with Tether to handle Bootstrap components
* Masonry.js with imagesLoaded to handle feed with offers

Webpack use entries for feed


## How to (examples):

### Creating button
1. In twig template create:
`<button class="btn btn-lg btn-primary customButton">Button text</button>` where btn-lg is button size (from bootstrap)
and btn-primary class is for button type - primary button has green background, secondary has blue, default is grey
2. In scss files you could also use mixin:
```
.customButton {
    @include button-variant(#fff, $primary-color, $primary-color);
    ...other styles
}
```
where first argument is button's text color, second argument is for background color, third is for border color