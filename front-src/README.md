# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

this is repo for building frontend templates when no real php platform is set up

### How do I get set up? ###

You need nodejs with npm installed (see https://nodejs.org/en/download/)

If you have it configured, (in terminal) go to main folder where do you have this repo cloned and run command:
```npm install```

Next you need run script:
```npm run build```
and open browser at address: http://localhost:3000

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact