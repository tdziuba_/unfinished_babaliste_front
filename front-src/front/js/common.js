/**
 * Created by Tomasz Dziuba on 30.12.2016.
 */

import Header from './components/Header';
import GoUpLink from './components/GoUpLink';
import { GeocoderHelper, Translator, ErrorLogger } from './helpers';
import BootstrapLoader from './vendor/bootstrap';
import ShotDownLayer from './components/ShotDownLayer';
import SendMeApp from './components/SendMeApp';
import MobileAppBanner from './components/MobileAppBanner';

window.cbApp = window.cbApp || {};
window.cbApp.ErrorLogger = ErrorLogger;

if (window && !window.onerror) {
	window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
		window.cbApp.ErrorLogger.fatal(errorMsg, url, lineNumber, column, errorObj);
	}
}

export function executeCommon() {
	new GoUpLink();

	new BootstrapLoader();

	// window.cbApp.GeocoderHelper = new GeocoderHelper();

	window.cbApp.Header = new Header();
	window.cbApp.Translator = new Translator();

	window.cbApp.Translator.parse();

	window.cbApp.getScrollbarWidth = () => {
		let fakeDiv = $('<div class="scrollbar-fake-div">').appendTo('body'),
			maxW = $('<div style="width: 100%">').appendTo(fakeDiv).outerWidth();

		fakeDiv.remove();

		return 100 - maxW;
	};

	cbApp.ShotDownLayer = new ShotDownLayer({ buttonSelector: '.js_show-door-layer' });

	cbApp.SendMeApp = new SendMeApp();

	cbApp.MobileAppBanner = new MobileAppBanner();

}

