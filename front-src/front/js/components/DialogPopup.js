/**
 * Created by Tomasz Dziuba on 06.02.2017.
 */
import ModalLayer from './ModalLayer';

/**
 * system messages popup based on bootstrap modal
 * usage:
 * let errorDialog = new DialogPopup({
		title: 'Form has errors',
		proceedActionLabel: 'Ok',
		content: 'Markera anledningen'
	});

 	errorDialog.setSuccessCallback(() => {
		alert('callback!');
	});

 errorDialog.getModal().modal('show');
 *
 */
export default class DialogPopup extends ModalLayer {

	onSaveCallback;

	constructor (options) {
		let o = $.extend({}, {
			showFooter			: true,
			showHeader			: true,
			showBody			: true,
			title				: 'System message',
			id					: 'cb-system-dialog',
			content				: '',
			cancelActionLabel	: null,
			proceedActionLabel	: 'Ok',
			destroyOnHide: true
		}, options);

		o.content = '<p class="system-message text-center">' + o.content + '</p>';

		super(o);

		this.handleEvents();
	}

	handleEvents () {
		let self = this;

		this.getModal().on('click', '.proceedBtn', (ev) => {
			self.onSuccess(ev);
		});

		this.getModal().on('hidden.bs.modal', () => {
			if (this.o.destroyOnHide) {
				self.destroyModal();
			}
		});
	}

	/**
	 * set the callback which will be executed when .proceedBtn is clicked
	 * @param callback
	 */
	setSuccessCallback (callback) {
		this.onSaveCallback = callback;
	}

	/**
	 * executes the callback
	 */
	onSuccess (ev) {

		if ( this.onSaveCallback ) {
			ev.preventDefault();
			ev.stopImmediatePropagation();

			this.onSaveCallback(ev);
		}

	}

}
