/**
 * Created by Tomasz Dziuba on 29.12.2016.
 */

let cbApp = cbApp || window.cbApp;

export default class FBHelper {
	FB;
	constructor(props) {
		this.props = props;

		this.init();
	}

	init() {
		let self = this;

		window.fbAsyncInit = function() {
			if (typeof window.FB !== 'undefined') {
				self.setFB(window.FB);

				this.FB.init({
					appId: (typeof cbApp.fb_appId !== 'undefined') ? cbApp.fb_appId : 401652553214093,
					xfbml: true,
					version: 'v2.8'
				});
				this.FB.AppEvents.logPageView();
			}
		};

		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/se_SV/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		return this;
	}

	setFB (fb) {
		this.FB = fb;
	}

	share(callback) {

		if (this.FB) {

			this.FB.ui(
				{
					method: 'share',
					href: document.location.href
				}, () => {
					if (callback) callback();
				});

		}

	}

}
