/**
 * Created by Tomasz Dziuba on 03.01.2017.
 */

export default class GoUpLink {

	constructor() {
		let gouplink = $('#gouplink');

		if (gouplink.length) {

			$('#gouplink:not(":disabled")').on('click', function() {
				$('html, body').animate({ scrollTop: 0 }, 300);
			});

			$(window).scroll(function() {
				if ($(window).scrollTop() >= 900) {
					if (gouplink.hasClass('hidden')) {
						gouplink.removeClass('hidden');
					}
				} else {
					if (!gouplink.hasClass('hidden')) {
						gouplink.addClass('hidden');
					}
				}
			});

		}
	}
}
