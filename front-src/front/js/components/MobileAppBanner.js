/**
 * Created by Tomasz Dziuba on 28.02.2017.
 */

import { getCookie, setCookie } from '../helpers/Utils';

export default class MobileAppBanner {

	btn;

	constructor (options) {
		this.o = $.extend({}, {
			containerSelector	: '#additionalStickyAppBanner',
			btnSelector			: '.downloadAppBtn',
			closeBtnSelector	: '.closeBtn',
			cookieName			: 'hideMobileAppBanner'
		}, options);

		this.oSelector = $(this.o.containerSelector);
		
		this.handleEvents();
	}

	handleEvents () {
		let self = this;

		if (this.oSelector.length) {
			this.btn = $(this.o.btnSelector, this.oSelector);

			// download button click
			this.btn.on('click', (ev) => {
				ev.preventDefault();

				self.onSuccess();
			});

			// close button click
			$(this.o.closeBtnSelector, this.oSelector).on('click', () => {
				self.onClose();
			});
		}

	}

	onSuccess () {
		setCookie(this.o.cookieName, 1, 365);

		this.hideBanner();

		window.location.href = this.btn.attr('href');
	}

	onClose() {
		setCookie(this.o.cookieName, 1);
		console.log(getCookie(this.o.cookieName));

		this.hideBanner();
	}

	hideBanner () {
		this.oSelector.addClass('fade');
	}

}
