/**
 * Created by Tomasz Dziuba on 30.01.2017.
 */
import handlebars from 'handlebars';
import Observer from '../helpers/Observer';

export default class ModalLayer {

	constructor (options) {
		this.tpl = handlebars.compile( $('#modalTpl').html() );
		this.o = $.extend({
			id: '',
			cssClass: '',
			content: '',
			showCloseBtn: true,
			showHeader: false,
			showFooter: false,
			showBody: false,
			cancelActionLabel: null,
			cancelActionId: null,
			proceedActionLabel: null,
			proceedActionId: null,
			modalOptions: {
				backdrop: true,
				keyboard: true,
				focus: true,
				show: false
			}
		}, options);

		if (this.o.id === '') {
			this.o.id = this.getRandomId();
		}

		this.observer = new Observer(this);

		let self = this;

		this.generateModal();

		if (this.modal && this.modal.length) {

			this.getModal().on('show.bs.modal', () => {
				$('#cbHeader').css('padding-right', cbApp.getScrollbarWidth());
			});

			this.getModal().on('shown.bs.modal', () => {
				self.observer.notify('open');
			});

			this.getModal().on('hidden.bs.modal', () => {
				self.observer.notify('hidden');
				$('#cbHeader').removeAttr('style');
			});
		}

	}

	/**
	 * generates id for modal if not assigned in options
	 * @returns {string}
	 */
	getRandomId () {
		let date = new Date();
		return 'modalId_' + date.getTime();
	}

	/**
	 * set new options
	 * @param options
	 * @returns {ModalLayer}
	 */
	setOptions (options) {
		this.o = $.extend({}, this.o, options);

		return this;
	}

	/**
	 * set content for .modal-content only and regenerate modal
	 * @param content
	 */
	setContent (content) {
		this.o.content = content;

		this.generateModal();
	}

	/**
	 * replace whole modal with new content
	 * @param html
	 */
	replaceModalContent (html) {
		this.getModal().html(html);

		this.generateModal();
	}

	/**
	 * append modal to body or update if exists
	 * @returns {ModalLayer}
	 */
	generateModal () {

		if ( !$('#' + this.o.id).length ) {
			$('body').append(this.tpl(this.o));
		} else {
			let newContent = $(this.tpl(this.o)).html();

			$('#' + this.o.id).html(newContent);
		}

		this.modal = $('#' + this.o.id).modal(this.o.modalOptions);

		return this;

	}

	/**
	 * remove modal from body
	 */
	destroyModal () {
		this.getModal().modal('hide');
		this.getModal().remove();

		this.observer.notify('destroyed');
	}

	/**
	 * returns modal object
	 * @returns {*|jQuery}
	 */
	getModal() {
		if (!this.modal) {
			this.modal = $('#' + this.o.id).modal();
		}

		return this.modal;
	}

	/**
	 * add listener to the observer listener list
	 * @param listener
	 */
	addListener (listener) {
		this.observer.add(listener);
	}

}
