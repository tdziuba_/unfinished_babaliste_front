/**
 * Created by Tomasz Dziuba on 22.02.2017.
 */

import ModalLayer from './ModalLayer';
import handlebars from 'handlebars';
import Translator from '../helpers/Translator';
import { setCookie } from '../helpers/Utils';

export default class SendMeApp {

	layer;
	btn;
	translator;
	formTpl;
	confirmationTpl;
	wasSent = false;

	constructor (options) {
		this.o = $.extend({}, {
			btnSelector				: '#sendMeAppScreenBtn',
			formTplSelector			: '#sendAppFormTemplate',
			confirmaionTplSelector	: '#sendAppConfirmTemplate'
		}, options);

		this.btn = $(this.o.btnSelector);
		this.translator = new Translator();
		this.formTpl = this.getTemplate(this.o.formTplSelector);
		this.confirmationTpl = this.getTemplate(this.o.confirmaionTplSelector);

		if (this.formTpl) {

			this.layer = new ModalLayer({
				id: 'sendMeAppLayer',
				content: this.formTpl( this.getFormData() ),
				showHeader: false,
				showFooter: true,
				showBody: true,
				proceedActionLabel: this.translator.translate('send_me_free_application')
			});

			this.layer.addListener(this);

			this.btn.removeClass('processing');
		}

		this.handleEvents();
	}

	getTemplate (selector) {
		let html = $(selector).html();

		return (html) ? handlebars.compile(html) : null;
	}

	getFormData () {
		return {
			legend: this.translator.translate('send_me_babaliste_application_for_android'),
			yourName: this.translator.translate('your_name'),
			yourEmail: this.translator.translate('your_e_mail')
		}
	}

	handleEvents () {
		let self = this;

		this.btn.on('click', () => {
			self.openFormLayer();
		});

		if (this.layer) {

			let form = $('.sendMeAppPopupBox', this.layer.getModal()),
				formBtn = $('.proceedBtn', this.layer.getModal());

			form.on('keyup change', () => {
				if (self.isFormValid()) {
					formBtn.removeAttr('disabled');
				} else {
					formBtn.attr('disabled', 'disabled');
				}
			});

			form.on('submit', (ev) => {
				ev.preventDefault();
				ev.stopImmediatePropagation();

				if (self.isFormValid()) {
					self.onSuccess();
				} else {
					formBtn.attr('disabled', 'disabled');
					self.onError();
				}
			});

			formBtn.on('click', (ev) => {
				ev.preventDefault();
				ev.stopImmediatePropagation();

				if (self.isFormValid()) {
					self.onSuccess();
				} else {
					formBtn.attr('disabled', 'disabled');
					self.onError();
				}

			});

			this.layer.getModal().on('click', '#closeConfirmBtn', () => {
				self.replaceBanner();
			})



		}
	}

	/**
	 * function called by ModalLayer if change it state
	 */
	update (sender, type) {
		$('.nameInput', this.layer.getModal()).focus();

		if (type === 'hidden' && this.wasSent) {
			this.replaceBanner();
		}

	}

	isFormValid () {
		let name = $('.nameInput', this.layer.getModal()).val(),
			email = $('.emailInput', this.layer.getModal()).val();

		return (name !== '' && email !== '');
	}

	openFormLayer () {

		try {
			this.layer.getModal().modal('show');
		} catch (err) {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
		}

	}

	/**
	 * submit form with ajax request and then show confirmation or error screen
	 */
	onSuccess () {
		let self = this,
			name = $('.nameInput', this.layer.getModal()).val(),
			email = $('.emailInput', this.layer.getModal()).val(),
			btn = $('.proceedBtn', this.layer.getModal());

		btn.addClass('processing').attr('disabled', 'disabled');

		try {

			$.ajax({
				url: '/', //@TODO Maciek trzeba zmienić url na właściwy
				method: 'post',
				data: {
					name: name,
					email: email,
				}
			}).done((resp) => {

				self.showConfirmation();

			}).fail((xhr, textStatus, errorThrown) => {

				if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(xhr);

				window.cbApp.ErrorLogger.error('Send me app form backend error: `' + xhr.status + ' ' + xhr.statusText + '`');

				self.layer.getModal().modal('hide');
			});

		} catch (err) {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
		}

	}

	onError () {

	}

	/**
	 * show confirmation screen
	 */
	showConfirmation () {
		this.layer.o = $.extend({}, this.layer.o, {
			proceedActionLabel: this.translator.translate('shot_down'),
			proceedActionId: 'closeConfirmBtn',
			showFooter: true,
			content: this.confirmationTpl({
				legend: this.translator.translate('an_application_download_link_was_sent_on_your_e_mail'),
				confirmText: this.translator.translate('follow_the_instruction_from_e_mail_to_install_app'),
				confirmSubinfo: this.translator.translate('if_you_don_t_receive_e_mail_check_spam_folder'),
			})
		});

		this.layer.generateModal();

		this.wasSent = true;

		// form was sent, send me app banner is hidden for a week

		try {
			setCookie('hideSendMeAppForm', 1, 7);
		} catch (err) {}

	}

	/**
	 * remove send me app banner and show #sendMeAppC2A
	 */
	replaceBanner () {
		$('#sendMeAppScreen').remove();
		$('#sendMeAppC2A').removeClass('hidden');
	}
}
