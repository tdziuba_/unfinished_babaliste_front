/**
 * Created by Tomasz Dziuba on 24.02.2017.
 */

import handlebars from 'handlebars';
import DialogPopup from '../DialogPopup';

export default class CopyToClipboard {

	btn;
	layer;
	tpl;
	confirmTpl;

	constructor (options) {
		this.o = $.extend({}, {
			btnSelector: 		'#offerShareByLink',
			tplSelector: 		'#copyToClipboardPopupTemplate',
			confirmTplSelector: '#copyToClipboardConfirmTemplate'
		}, options);

		this.btn = $(this.o.btnSelector);
		this.tpl = this.getTemplate(this.o.tplSelector);
		this.confirmTpl = this.getTemplate(this.o.confirmTplSelector);
		this.layer = new DialogPopup(this.getLayerData());

		this.layer.setSuccessCallback( this.copyToClipboard.bind(this) );

		this.setLayerContent();


		//this.layer.generateModal();
		this.layer.addListener(this);
		
		this.handleEvents();
	}

	getTemplate (selector) {
		let html = $(selector).html();

		return (html) ? handlebars.compile(html) : null;
	}

	getLayerData () {
		return {
			id: 				'copyToClipboardLayer',
			cssClass: 			'copyToClipboardBox text-center',
			title: 				'<strong>Appuyez sur CTRL + C, ENTER pour copier l\'url:</strong>',
			proceedActionId: 	'copyToClipboardWrapperBtn',
			proceedActionLabel: 'Copier le lien »',
			content: 			'',
			destroyOnHide: 		false
		}
	}

	setLayerContent () {
		this.layer.setContent(
			this.getLayerContent()
		);
	}

	getLayerContent () {
		return this.tpl({
			successText: 'Le lien vers l\'annonce a été copié !'
		});
	}

	handleEvents () {
		let self = this;

		this.btn.off().on('click', (ev) => {
			ev.preventDefault();

			self.layer.getModal().modal('show');
		});
	}

	copyToClipboard (ev) {
		ev.preventDefault();
		ev.stopImmediatePropagation();

		let popupEl = this.layer.getModal(),
			area, copied;

		if (popupEl) {
			area = popupEl.find('#copyToClipboardArea');

			area.focus();
			area.select();

			try {
				copied = document.execCommand('copy');

				if (!copied) {
					popupEl.modal('hide');
					window.prompt('Appuyez sur CTRL + C, ENTER pour copier l\'url', this.btn.data('url'));
				} else {

					this.setConfirmLayer(popupEl);
				}

			} catch(err) {
				if (typeof console !== 'undefined' && typeof console.warn == 'function') console.warn(err);
			}

		}

	}

	setConfirmLayer (popupEl) {
		this.layer.setOptions({
			showHeader: false,
			showBody: true,
			showFooter: true,
			proceedActionLabel: 'Ok',
			proceedActionId: 'copyToClipboardCloseBtn',
			content: this.confirmTpl({
				successText: 'Le lien vers l\'annonce a été copié !'
			})
		});

		this.layer.generateModal();

		$('#copyToClipboardCloseBtn', popupEl).on('click', () => {
			popupEl.modal('hide');
		});
	}

	update (sender, layerState) {

		if (layerState == 'hidden') {

			sender.setOptions(
				$.extend({}, this.getLayerData(), {
					showHeader: true,
					showBody: true,
					content: this.getLayerContent()
				})
			);
			sender.generateModal();

		}
	}

}
