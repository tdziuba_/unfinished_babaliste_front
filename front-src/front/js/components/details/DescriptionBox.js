/**
 * Created by Tomasz Dziuba on 03.02.2017.
 */

/**
 * toggle description container to show full content or short version
 */
export default class DescriptionBox {
	button;
	selector;
	textBox;
	initHeight;
	isExpanded = false;

	constructor (options) {
		this.o = $.extend({}, {
			parent					: '#detailsDescriptionBox',
			selector				: '.description',
			seeMoreBtnSelector		: '.seeMore',
			textSelector			: '.description-text',
			fullHeightClassname		: 'readable',
			smallHeightClassname	: 'short',
			useCssAnimation			: true
		}, options);

		this.selector = $(this.o.selector, this.o.parent);
		this.button = $(this.o.seeMoreBtnSelector, this.o.parent);
		this.textBox = $(this.o.textSelector, this.o.parent);
		this.initHeight = this.selector.height();

		if (this.button.length) {
			this.selector.css('margin-bottom', this.button.height() / 2);
			this.button.removeClass('invisible');
		}

		this.handleEvents();
	}

	handleEvents () {
		let self = this;

		this.button.off('click').on('click', (ev) => {
			ev.preventDefault();

			self.handleCssClass();
			self.replaceButtonText();

			if (!self.o.useCssAnimation) {
				self.animateContainer();
			} else {
				self.setIsExpanded();
			}

		});
	}

	setIsExpanded () {
		this.isExpanded = (!this.isExpanded);
	}

	handleCssClass () {

		if (!this.isExpanded) {

			this.selector.addClass(this.o.fullHeightClassname);

		} else {

			this.selector.removeClass(this.o.fullHeightClassname);

		}

		return this;
	}

	replaceButtonText () {
		let content = this.button.find('.seeMoreText'),
			text;

		if (!this.isExpanded) {

			text = cbApp.Translator.translate('details.readLess');
			content.html('<span class="translation">' + text + '</span> <i class="icon icon-angle-up"></i>');

		} else {

			text = cbApp.Translator.translate('details.readMore');
			content.html('<span class="translation">' + text + '</span> <i class="icon icon-angle-down"></i>');

		}

		return this;
	}

	animateContainer () {
		let self = this,
			height = this.textBox.outerHeight(true);

		if (this.isExpanded) {
			height = this.initHeight;
		}

		this.selector.animate({maxHeight: height}, 350, () => {
			self.setIsExpanded();
		});
	}

}