/**
 * Created by Tomasz Dziuba on 30.01.2017.
 */

import ModalLayer from '../ModalLayer';
import handlebars from 'handlebars';
import Hammer from '../../vendor/hammer';

export default class FullScreenGallery {
	currentPhoto = 0;
	tpl;
	dGallery;
	imgLength;

	constructor (options) {

		this.o = $.extend({
			dgSelector: 				'#details-gallery',
			dgThumbnailSelector: 		'.thumbnail-link',
			dgPictureSelector: 			'.js-offer-picture',
			fsGallerySelector: 			'#fsGallery',
			fsThumbsWrapperSelector: 	'.thumbnails-wrapper',
			fsThumbnailsSelector: 		'.thumbnail-link',
			fsPicWrapperSelector: 		'.pictureWrapper',
			fsPictureSelector: 			'.main-picture',
			fsArrowSelector: 			'.fsGalleryBtn'
		}, options);

		this.modal = new ModalLayer({
			id: 'fsGalleryModal',
			showCloseBtn: false
		});

		this.dGallery = $(this.o.dgSelector);
		this.fsGallery = this.getMainSelector();

		this.tpl = handlebars.compile( $('#fsGalleryTpl').html() );

		this.modal.addListener(this);

	}

	setCurrentPhoto (index) {
		this.currentPhoto = index;

		if (this.modal.getModal().find(this.o.fsGallerySelector).length) {
			this.changePicture(index);
		}

	}

	getMainSelector () {
		if (!this.fsGallery || !this.fsGallery.length) {
			this.fsGallery = $(this.o.fsGallerySelector, '#bodyId');
		}

		return this.fsGallery;
	}

	update (sender, type) {
		let self = this;

		switch (type) {
			case 'open':
				self.imageOnLoad();
				self.calculateImageSize();
				self.handleEvents();
				break;
			case 'destroyed':
				break;
			case 'hidden':
				self.modal.getModal().find('.image-box').addClass('processing');
				break;
		}
	}

	recalculateIndex (direction) {
		let index = parseInt( this.currentPhoto + direction );

		if ( index >= this.imgLength ) {
			index = 0;
		}

		if ( index < 0 ) {
			index = this.imgLength - 1;
		}

		return index;
	}

	getThumbnails () {
		let self = this,
			thumbs = this.dGallery.find(this.o.dgThumbnailSelector);

		let data = thumbs.map((index) => {

			if (typeof index === 'number') {

				let t = $(thumbs[index]),
					img = t.find('img');

				return {
					index: index,
					selected: (index === self.currentPhoto),
					thumbSrc: img.attr('src'),
					imgSrc: t.attr('href'),
					title: img.attr('alt')
				};
			}

		});

		return $(data).toArray();

	}

	getPictures () {
		let self = this,
			pics = this.dGallery.find(this.o.dgPictureSelector);

		let data = pics.map((index) => {

			if (typeof index === 'number') {
				let img = $(pics[index]);

				return {
					index: index,
					selected: (index === self.currentPhoto),
					imgSrc: img.attr('src'),
					title: img.attr('alt')
				};
			}

		});

		return $(data).toArray();

	}

	changePicture (index) {
		let selector = this.getMainSelector(),
			pictures = selector.find(this.o.fsPictureSelector),
			thumbs = selector.find(this.o.fsThumbnailsSelector),
			imgBox = selector.find('.image-box');

		imgBox.addClass('processing');

		// setting active image
		$(pictures).addClass('hidden').removeAttr('style');
		$(pictures).eq(index).removeClass('hidden');

		// setting active thumbnail
		$(thumbs).removeClass('selected');
		$(thumbs).eq(index).addClass('selected');

		this.imageOnLoad( $(pictures).eq(index) );

	}

	show () {

		try {

			if ( !this.modal.getModal().find(this.o.fsGallerySelector).length ) {
				this.modal.setContent(this.tpl({
					thumbnails: this.getThumbnails(),
					pictures: this.getPictures(),
					isLoading: true
				}));
			}

			this.fsGallery = this.getMainSelector();
			this.modal.getModal().modal('show');


		} catch (err) {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
		}

	}

	calculateImageSize () {

		try {

			let selector = this.getMainSelector(),
				img = selector.find(this.o.fsPictureSelector).eq(this.currentPhoto),
				thumbsWrapper = selector.find(this.o.fsThumbsWrapperSelector),
				picWrapper = selector.find(this.o.fsPicWrapperSelector);

			let maxH = $(window).outerHeight() - thumbsWrapper.outerHeight() - 40,
				maxW = picWrapper.width() - 40,
				fitbyWidth = ((maxW / img.width()) < (maxH / img.height())) ? true : false;

			picWrapper.css('height', maxH);

			img.css({
				height: ( fitbyWidth ) ? 'auto' : maxH,
				width: ( fitbyWidth ) ? maxW : 'auto'
			});

		} catch (err) {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
		}

	}

	handleEvents () {
		let self = this,
			t;

		this.imgLength = $(this.o.fsPictureSelector).length;

		$(window).on('resize', () => {
			if (typeof t !== 'undefined') {
				clearTimeout(t);
			}

			t = setTimeout(() => {
				self.calculateImageSize();
			}, 250);
		});

		this.handleThumbnails();
		this.handleArrows();
		this.handleKeyboardEvents();
	}

	handleThumbnails () {
		let self = this;

		$(this.o.fsThumbnailsSelector, this.o.fsGallerySelector).off('click').on('click', (ev) => {
			ev.preventDefault();

			let thumb = $(ev.currentTarget),
				index = thumb.data('index');

			self.setCurrentPhoto(index);
		});
	}

	handleArrows () {
		let self = this,
			hm = new Hammer( this.fsGallery[0].querySelector(this.o.fsPicWrapperSelector) );

		$(this.o.fsArrowSelector, this.o.fsGallerySelector).off('click').on('click', (ev) => {
			let arrow = $(ev.currentTarget),
				index = self.recalculateIndex( arrow.data('direction') );

			self.setCurrentPhoto(index);
		});

		hm.on('swipeleft', () => {
			self.setCurrentPhoto( self.recalculateIndex(-1) );
		});

		hm.on('swiperight', () => {
			self.setCurrentPhoto( self.recalculateIndex(1) );
		});
	}

	handleKeyboardEvents () {
		let self = this;

		$(document).on('keyup', (ev) => {

			// next image (arrow right is pressed)
			if (ev.which === 39) {
				self.setCurrentPhoto( self.recalculateIndex(1) );
			} else if (ev.which === 37) {
				// previous image (arrow left)
				self.setCurrentPhoto( self.recalculateIndex(-1) );
			}

		});
	}

	imageOnLoad (pic) {
		let self = this,
			imgBox = this.modal.getModal().find('.image-box'),
			img = new Image();

		if (!pic) {
			pic = imgBox.find(this.o.fsPictureSelector).eq(this.currentPhoto);
		}

		img.onload = () => {
			self.calculateImageSize();
			imgBox.removeClass('processing');
			pic.removeClass('hidden');
		};
		img.src = pic.attr('src');

	}

}
