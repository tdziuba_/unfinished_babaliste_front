/**
 * Created by Tomasz Dziuba on 30.01.2017.
 */

import FullScreenGallery from './FullScreenGallery';
import handlebars from 'handlebars';
import Hammer from '../../vendor/hammer';

export default class Gallery {
	fsGallery;
	oSelector;
	oMainWrp;
	imgLength = 0;
	currentIndex = 0;

	constructor (options) {
		this.fsGallery = new FullScreenGallery();

		this.o = $.extend({
			selector: 					'#details-gallery',
			bulletsSelector: 			'.bullets',
			singleBulletSelector: 		'.bullet',
			thumbnailsSelector: 		'.thumbnails',
			singleThumbnailSelector: 	'.thumbnail-link',
			mainImgWrapper: 			'#galleryImgWrapper',
			mainImgSelector: 			'.js-offer-picture',
			arrowSelector: 				'.js-gallery-nav-btn'
		}, options);

		this.oSelector = $(this.o.selector);
		this.oMainWrp = $(this.o.mainImgWrapper);

		this.imgLength = this.oSelector.find(this.o.singleThumbnailSelector).length;

		if (this.imgLength) {
			this.generateArrows();
		}

		this.handleBulletEvents();
		this.handleThumbnailsEvents();
		this.handleMainImageEvents();
	}

	generateArrows () {
		let source = $('#galleryNavTpl').html(),
			tpl = handlebars.compile(source);

		this.oMainWrp.append(tpl());

		this.handleArrowEvents();
	}

	handleThumbnailsEvents () {
		let self = this;

		this.oSelector.on('click', self.o.singleThumbnailSelector, (ev) => {
			ev.preventDefault();

			let thumb = $(ev.currentTarget),
				index = thumb.data('index');

			self.changePicture(index);

		});
	}

	handleBulletEvents () {
		let self = this;

		this.oSelector.on('click', self.o.singleBulletSelector, (ev) => {
			ev.preventDefault();

			let bullet = $(ev.currentTarget),
				index = bullet.data('index');

			self.changePicture(index);

		});
	}

	handleArrowEvents () {
		let self = this,
			hm = new Hammer( this.oSelector[0].querySelector(this.o.mainImgWrapper) );

		this.oSelector.on('click', self.o.arrowSelector, (ev) => {
			let arrow = $(ev.currentTarget),
				index = self.recalculateIndex( arrow.data('direction') );

			self.changePicture(index);
		});

		hm.on('swipeleft', () => {
			self.changePicture( self.recalculateIndex(-1) );
		});

		hm.on('swiperight', () => {
			self.changePicture( self.recalculateIndex(1) );
		});

		this.handleKeyboardEvents();

	}

	handleKeyboardEvents () {
		let self = this,
			isOnHover = false;

		self.oMainWrp.hover(
			() => { isOnHover = true; },
			() => { isOnHover = false; }
		);

		$(document).on('keyup', (ev) => {

			if (isOnHover) {

				// next image (arrow right is pressed)
				if (ev.which === 39) {
					self.changePicture( self.recalculateIndex(1) );
				} else if (ev.which === 37) {
					// previous image (arrow left)
					self.changePicture( self.recalculateIndex(-1) );
				}

			}

		});
	}

	recalculateIndex (direction) {
		let index = parseInt( this.currentIndex + direction );

		if ( index >= this.imgLength ) {
			index = 0;
		}

		if ( index < 0 ) {
			index = this.imgLength - 1;
		}

		return index;
	}

	handleMainImageEvents () {
		let self = this;

		this.oMainWrp.on('click', this.o.mainImgSelector, (ev) => {
			let index = $(ev.currentTarget).data('index');

			self.fsGallery.setCurrentPhoto(index);
			self.fsGallery.show();
		});
	}

	changePicture (index) {
		let bullets = this.oSelector.find(this.o.singleBulletSelector),
			thumbs = this.oSelector.find(this.o.singleThumbnailSelector);

		this.setCurrentIndex(index);
		this.fsGallery.setCurrentPhoto(index);

		// setting active image
		this.oMainWrp.find(this.o.mainImgSelector).addClass('hidden');
		this.oMainWrp.find(this.o.mainImgSelector).eq(index).removeClass('hidden');

		// setting active thumbnail
		thumbs.removeClass('selected');
		thumbs.eq(index).addClass('selected');

		// setting active bullet
		bullets.removeClass('selected');
		bullets.eq(index).addClass('selected');
	}

	setCurrentIndex (index) {
		this.currentIndex = index;
	}
}
