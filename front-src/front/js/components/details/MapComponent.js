/**
 * Created by Tomasz Dziuba on 03.02.2017.
 */

export default class MapComponent {

	coords;

	constructor (options) {
		this.o = $.extend({}, {
			selector: '#gmapOnDetails',
		}, options);

		this.selector = $(this.o.selector);

		this.handleEvents();
	}

	handleEvents () {
		if (this.selector.length) {
			this.setCoordinates();
			this.showStaticMap();
		}
	}

	setCoordinates () {
		if (this.selector.data('lat') && this.selector.data('lng')) {
			this.coords = {
				lat: this.selector.data('lat'),
				lng: this.selector.data('lng')
			};
		} else {
			this.coords = cbApp.GeocoderHelper.getCoordinates();
		}
	}

	showStaticMap () {
		var self = this,
			url = '//maps.googleapis.com/maps/api/staticmap?',
			width = (this.selector.innerWidth() < 640) ? this.selector.innerWidth() : 640,
			height = (this.selector.innerWidth() < 640) ? 180 : this.selector.innerHeight();

		url += 'center=' + this.coords.lat + ',' + this.coords.lng;
		url += '&zoom=13&size=' + parseInt(width) + 'x' + parseInt(height);
		url += '&path=color:0x2AB4E2FF|fillcolor:0x2AB4E2|weight:2';
		url += this.getStaticMapCircle(900);
		url += '&key=' + cbApp.gmap_key;

		this.selector.removeClass('processing').html('<img class="img-full" src="' + url + '" alt="">');
	}

	/**
	 * generates circle on static map to show location area see: http://stackoverflow.com/questions/7316963/drawing-a-circle-google-static-maps#answer-35660617
	 * @param rad
	 * @returns {string}
	 */
	getStaticMapCircle (rad) {
		var r = 6371,
			pi = Math.PI,
			_lat = (this.coords.lat * pi) / 180,
			_lng = (this.coords.lng * pi) / 180,
			d = (rad/1000) / r,
			i = 0,
			paramsStr = '';

		for (i = 0; i <= 360; i+= 8) {
			var brng = i * pi / 180;

			var pLat = Math.asin(Math.sin(_lat) * Math.cos(d) + Math.cos(_lat) * Math.sin(d) * Math.cos(brng));
			var pLng = ((_lng + Math.atan2(Math.sin(brng) * Math.sin(d) * Math.cos(_lat), Math.cos(d) - Math.sin(_lat) * Math.sin(pLat))) * 180) / pi;
			pLat = (pLat * 180) / pi;

			paramsStr += "|" + pLat + "," + pLng;
		}

		return paramsStr;
	}

}
 