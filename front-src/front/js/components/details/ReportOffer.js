/**
/**
 * Created by Tomasz Dziuba on 06.02.2017.
 */

import ModalLayer from '../ModalLayer';
import handlebars from 'handlebars';
import DialogPopup from '../DialogPopup';

export default class ReportOffer {

	button;
	offerId;
	form;

	constructor (options) {
		this.o = $.extend({}, {
			tplSelector		: '#reportOfferTemplate',
			formSelector	: '#reportOfferBox',
			buttonSelector	: '#reportOffer'
		}, options);

		this.tpl = this.getTemplate();
		this.button = $(this.o.buttonSelector);
		this.offerId = this.button.data('id');

		this.layer = new ModalLayer({
			id: 'reportOfferPopup'
		});
		this.layer.addListener(this);
		this.layer.setContent(this.tpl({ id: this.offerId }));

		this.handleEvents();
	}

	/**
	 * returns compiled template from injected to html source
	 * @returns {*}
	 */
	getTemplate () {

		if (!this.tpl) {
			let source = $(this.o.tplSelector).html();

			if (source) {
				return handlebars.compile(source);
			}

		} else {
			return this.tpl;
		}

		return;
	}

	/**
	 * handle ReportOffer popup various events
	 */
	handleEvents () {
		let self = this;
		this.form = $(this.o.formSelector);

		this.button.on('click', () => {
			self.layer.getModal().modal('show');
		});

		this.form.on('click', '.sendReportBtn', (ev) => {
			ev.preventDefault();

			self.validateForm(ev);
		});

		this.form.on('change', '.reportTypeInput', () => {
			self.form.addClass('selected');
		})

	}

	/**
	 * ModalLayer notify about it state changes and ReportOffer do correct action
	 * @param sender
	 * @param type
	 */
	update (sender, type) {
		if (type === 'hidden') {
			this.resetForm();
		}
	}

	/**
	 * reset form to base settings
	 */
	resetForm () {
		this.form.removeClass('selected');
		this.form.find('.reportTypeInput:checked').removeAttr('checked');
		this.form.find('#reportOfferReasonField').val('');
	}

	/**
	 * validates form - only type of abuse is required
	 * @param ev
	 */
	validateForm (ev) {
		ev.preventDefault();

		let radio = $('.reportTypeInput:checked', this.form);

		if (radio.length) {
			this.submitForm();
		} else {
			this.showError();
		}

	}

	/**
	 * displays system dialog with error
	 */
	showError () {
		let errorDialog = new DialogPopup({
			title: 'Form has errors',
			proceedActionLabel: 'Ok',
			content: cbApp.Translator.translate('please_select_reason')
		});

		errorDialog.getModal().modal('show');

	}

	/**
	 * displays thanks screen
	 */
	showConfirmation () {
		let self = this,
			confirmBox = $('#reportOfferBoxConfirmation');

		confirmBox.removeClass('hidden');
		$('#reportOfferBox').addClass('hidden');

		this.form.hide();

		setTimeout(function () {
			self.layer.getModal().modal('hide');
		}, 2000);
	}

	/**
	 * submit form by ajax call and displays confirmation
	 */
	submitForm () {
		let self = this;

		// tu będzie ajax na razie po prostu wyświetlam podziekowanie i zamykam layer
		this.showConfirmation()
	}
}
 