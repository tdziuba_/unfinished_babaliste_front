/**
 * Created by Tomasz Dziuba on 18.01.2017.
 */

import Masonry from '../../vendor/masonry.pkgd';
import imagesLoaded from '../../vendor/imagesloaded.pkgd';
import LoadMore from './LoadMore';

const masonryOptions = {
	itemSelector: '.js_board_item',
	percentPosition: true,
	columnWidth: '.js_board_item',
	isAnimated: false,
	gutter: 0,
	transitionDuration: 0,
	initLayout: true,
	stamp: '#cbSidebar'
};

export default class GridLayout {
	bricks;
	container;
	items;

	constructor (options) {
		this.o = $.extend({}, {
			containerSelector	: '.masonry-layout',
			itemSelector		: '.gridItem'
		}, options);

		try {

			this.container = $(this.o.containerSelector, '#cbContent');
			this.items = this.container[0].querySelectorAll(this.o.itemSelector);
			this.recalculate();

			this.bricks = new Masonry(this.container[0], masonryOptions);

			this.handleEvents();

			this.getBricks().layout();

			this.loadMore = new LoadMore({oGrid: this.container, oParent: this});

		} catch (err) {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
		}

	}

	getBricks () {
		return this.bricks;
	}

	handleEvents () {
		let self = this,
			imgL;

		this.getBricks().once('layoutComplete', () => {
			self.container.parent().removeClass('processing');
		});

		imgL = imagesLoaded( self.container[0], () => {
			self.getBricks().layout();
		});

		$(window).on('resize', () => {
			self.recalculate();
		});

		$(window).on('orientationchange', () => {
			self.recalculate();
		});

		imgL.on('progress', ( instance, image ) => {
			let parent = $(image.img).parents('.gridItem');

			if (parent.data('picRatio')) {
				window.cbApp.calculateImgHeight(parent[0]);
			}

		});

	}

	recalculate () {
		let self = this,
			t;

		if (typeof t !== 'undefined') clearTimeout(t);
		t = setTimeout(() => {
			window.cbApp.calculatePlaceholders(self.items, self.container[0]);
		}, 250);
	}

}

$(document).ready(() => {
	window.cbApp.GridLayout = new GridLayout();
});
