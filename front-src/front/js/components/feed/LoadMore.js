/**
 * Created by Tomasz Dziuba on 04.01.2017.
 */

import Handlebars from 'handlebars';
// import { isElementOnScreen } from '../../helpers/Utils';
import ErrorLogger from '../../helpers/ErrorLogger';

export default class LoadMore {
	o = {};
	offers = [];
	pages = 0;
	allowNextRequest = true;
	offerAppended = false;

	constructor (options) {
		this.o = $.extend({}, {
			oBtn: $('#loadMoreOffersBtn'),
			oGrid: $('.masonry-layout'),
			oParent: cbApp.GridLayout,
			tpl: '',
			offset: 32,
			limit: 32,
			counter: 32,
			location: null,
			category: null,
			searchText: null,
			pages: 0,
			currentPage: null,
			initiated: false,
			maxPages: 10, //how much pages with items are allowed before starting removing
			removingPages: true //remove or leave previously viewed page when maxPages are reached
		}, options);

		this.init();
	}

	init () {
		let self = this,
			addOfferBtn = $('#addOfferBtnFooter'),
			goUpBtn = $('#gouplink'),
			t;

		if (!this.o.initiated) {

			this.o.tpl = this.getTemplate();

			this.o.initiated = true;

			/**
			 * click load more button and appending more items
			 */
			this.o.oBtn.on('click', function () {
				let btn = $(this);

				btn.addClass('processing');

				self.o.category = btn.data('category');
				self.o.location = btn.data('location');
				self.o.searchText = btn.data('search-text');

				if (self.offers.length) {
					self.appendOffers();
				} else {
					self.loadMoreAction().appendOffers();
				}

			});

			$(window).on('scroll', function () {

				if (!addOfferBtn.is(':disabled')) {
					addOfferBtn.attr('disabled', 'disabled').addClass('disabled')
				}
				if (!goUpBtn.is(':disabled')) {
					goUpBtn.attr('disabled', 'disabled').addClass('disabled')
				}

				if (typeof t !== 'undefined') {
					clearTimeout(t);
				}

				t = setTimeout(function () {
					addOfferBtn.removeAttr('disabled').removeClass('disabled');
					goUpBtn.removeAttr('disabled').removeClass('disabled');

					self.handleScroll();
				}, 100);

			});


		}

	}

	/**
	 * returns handlebars template
	 * @returns {null}
	 */
	getTemplate () {
		let tpl = $('#gridItemTemplate').html();

		return (tpl) ? Handlebars.compile(tpl) : null;
	}

	/**
	 * allow to do next request on scroll
	 */
	allowRequest () {
		this.allowNextRequest = true;
	}

	/**
	 * disable next ajax requests
	 */
	disallowRequest () {
		this.allowNextRequest = false;
	}

	setOfferAppended (wareAppended) {
		if (typeof wareAppended === 'undefined') {
			wareAppended = true;
		}

		this.offerAppended = wareAppended;
	}

	/**
	 * load more offers via ajax
	 */
	loadMoreAction () {
		let self = this,
			data = {
				offset: self.o.offset,
				inp_category_id: self.o.category,
				inp_location_id: self.o.location,
				inp_text: self.o.searchText
			};

		if (this.allowNextRequest && !this.offerAppended) {

			$.ajax({
				url: '/',
				method: 'GET',
				data: data,
				dataType: 'json'
			})
				.done( (resp) => {
					self.o.offset += self.o.limit;

					self.setOfferAppended();

					if (resp.status == 'success' && resp.offers.length) {

						self.storeNextOffers(resp.offers);
						self.o.oBtn.removeClass('invisible processing');

						return self;

					} else {

						self.disallowRequest();
						self.o.oBtn.remove();

					}

				})
				.fail( () => {
					new ErrorLogger('request to /load-more failed on parameters: ' + JSON.stringify(data));
				});

		}

	}

	/**
	 * append offers to grid
	 */
	appendOffers () {
		var html = '',
			items = [],
			data = this.offers,
			msnry,
			frg = document.createDocumentFragment(),
			offset = $(window).scrollTop();

		if (data && data.length) {
			this.pages++;

			msnry = this.o.oParent.getBricks();

			for (var i = 0; i < data.length; ++i) {
				if (data[i].price == '0 DH') {
					data[i].price = 0;
				}

				if (i == data.length - 1) {
					data[i].isLast = true;
				}

				data[i].url = '/show/' + data[i].id;

				html += this.o.tpl(data[i]);
			}

			$('<div id="itemsPage-' + this.pages + '">' + html + '</div>').appendTo(frg);
			items = frg.querySelectorAll('.gridItem');

			this.o.oGrid.append($(frg));
			msnry.appended(items);

			$('html, body').animate({ scrollTop: offset }, 200);

			for (let i = 0; i < items.length; ++i) {

				if (items[i].dataset.picRatio) {
					window.cbApp.calculateImgHeight(items[i]);
					items[i].className += ' ready';
				} else {
					let img = new Image();

					$(img).on('load', () => {
						msnry.layout();
					});

					img.src = items[i].querySelector('.js-pictureImg').src;
				}

			}

			msnry.layout();

			this.clearNextOffers();

		}
	}

	/**
	 * hiding and showing 'item pages' with offers on scroll
	 */
	handleScroll () {
		let self = this;

		this.prepareNextContent();

		// if (this.pages > 2) {
		//
		// 	for (let i = 0; i < this.pages; ++i) {
		// 		let el = $('#itemsPage-' + i);
		//
		// 		if (el.length) {
		// 			console.log(el, isElementOnScreen(el));
		// 			if ( isElementOnScreen(el) ) {
		// 				self.showPage(el);
		// 			} else {
		// 				self.hidePage(el);
		// 			}
		// 		}
		// 	}
		//
		// }

	}

	/**
	 * hiding 'out of screen' page
	 * @param el
	 */
	hidePage (el) {
		$(el).addClass('invisible');
	}

	/**
	 * showing page which is 'on screen'
	 * @param el
	 */
	showPage (el) {
		this.currentPage = el;
		$(el).removeClass('invisible');
	}

	/**
	 * do a request for more offers before onclick action and prepare content to eliminate latency
	 * when user is clicking the button and waiting for response
	 */
	prepareNextContent () {
		if (this.allowNextRequest) {
			this.loadMoreAction();
		}
	}

	/**
	 * store temporaly offers to append after button click
	 * @param offers
	 */
	storeNextOffers (offers) {
		this.offers = offers;
	}

	/**
	 * clear offers array
	 */
	clearNextOffers () {
		this.offers = [];

		this.setOfferAppended(false);
	}

}
