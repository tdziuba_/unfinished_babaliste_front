/**
 * Created by Tomasz Dziuba on 19.01.2017.
 */
import Observer from '../../helpers/Observer';

export default class Menu {

	constructor (options) {
		this.o = $.extend({}, {
			menu: $('#categoryMenu'),
			activator: '.root .area',
			openClassName: 'open'
		}, options);

		this.observer = new Observer();

		this.handleEvents();
	}

	handleEvents () {
		let self = this;

		this.o.menu.off('click').on('click', this.o.activator, (ev) => {
			ev.preventDefault();

			if (self.isOpen()) {
				self.close();
			} else {
				self.open();
			}

		});
	}

	isOpen () {
		return this.o.menu.hasClass('open');
	}

	addListener (listener) {
		this.observer.add(listener);
	}

	update (sender, isOppositeOpen) {
		if (isOppositeOpen) {
			this.close();
		}
	}

	open () {
		let self = this;

		this.o.menu.addClass(this.o.openClassName);

		this.observer.notify(self.isOpen());
	}

	close () {
		this.o.menu.removeClass(this.o.openClassName);
	}

	// getBackButton () {
	// 	let oBackBtn = this.o.menu.find('.goBackBtn'),
	// 		backUrl = this.o.menu.find('.selected-box .area').attr('href');
	//
	// 	if (backUrl == window.location.pathname) {
	// 		backUrl = '/hela-sverige';
	// 	}
	//
	// 	if ( !oBackBtn.length ) {
	// 		oBackBtn = $('<a href="' + backUrl + '" class="btn goBackBtn">Retourner <i class="icon icon-angle-left"></i></a>');
	// 	}
	//
	// 	return oBackBtn;
	// }


}
