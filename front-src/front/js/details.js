/**
 * Created by Tomasz Dziuba on 28.12.2016.
 */

import * as c from './common';
import Gallery from './components/details/Gallery';
import DescriptionBox from './components/details/DescriptionBox';
import MapComponent from './components/details/MapComponent';
import ReportOffer from './components/details/ReportOffer';
import CopyToClipboard from './components/details/CopyToClipboard';
import FBHelper from './components/FbHelper';
import '../scss/details.scss';

$(document).ready( () => {

	c.executeCommon();

	cbApp.Gallery = new Gallery();
	cbApp.DescriptionBox = new DescriptionBox();
	cbApp.MapComponent = new MapComponent();
	cbApp.ReportOffer = new ReportOffer();
	cbApp.CopyToClipboard = new CopyToClipboard();

	$('#backToOffersBtn').on('click', (ev) => {
		ev.preventDefault();

		if (history.length === 1){
			window.location = '/' + $(this).data('backUrl');
		} else {
			history.back();
		}

	});

	/**
	 * shares on Facebook
	 * @type {FBHelper}
	 */
	let fb = new FBHelper();

	$('#offerShareItemOnFbBtn').on('click', () => {
		fb.share();
	});

});
