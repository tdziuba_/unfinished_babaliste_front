/**
 * Created by Tomasz Dziuba on 28.12.2016.
 */
import '../scss/feed.scss';
import FBHelper from './components/FbHelper';
import * as c from './common';
import Menu from './components/feed/Menu.js';

$(document).ready( () => {
	// here will be code execution on document.readyState == 'complete'

	try {

		let fb = new FBHelper();

		$('#shareBtn').on('click', function () {
			fb.share(function () {
				alert('callback po wykonaniu share\'a');
			});
		});

		if ($('.cb-section-top').find('.cntTree').length) {

			let categoryMenu = new Menu({
				menu: $('#categoryMenu')
			});

			let locationMenu = new Menu({
				menu: $('#locationMenu')
			});

			categoryMenu.addListener(locationMenu);
			locationMenu.addListener(categoryMenu);

		}

		c.executeCommon();

	} catch (err) {
		if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
	}

});
