/**
 * Created by Tomasz Dziuba on 20.01.2017.
 */

let instance;

class ErrorLoggerSingleton {
	time;
	logger;

	constructor () {
		this.time = this.getCurrentTime();

		if(!instance){
			instance = this;
		}

		return instance;
	}

	getInstance () {
		return instance;
	}

	info (data, info) {

	}

	warn (data, info) {

	}

	error (info, error) {
		let logObj = {
			msg: info,
			details: error
		};
		this.log(logObj);
	}

	fatal (errorMsg, url, lineNumber, column, errorObj) {
		let fatal = {
			"msg": "Uncaught Exception",
			"errorMsg": errorMsg,
			"url": url,
			"line number": lineNumber,
			"column": column
		};

		console.log(fatal);
		this.log(fatal);
	}

	log (logObj) {
		let self = this;

		try {

			$.ajax({
				type: 'POST',
				url: '/error/js-error',
				data: JSON.stringify({
					time: self.time,
					details: logObj,
					userAgent: navigator.userAgent
				}),
				contentType: 'application/json; charset=utf-8'
			});

		} catch (err) {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
		}
	}

	getCurrentTime () {
		let c_date = new Date();
		let error_time = "Date: " + c_date.getDate() + "."
			+ (c_date.getMonth() + 1) + "."
			+ c_date.getFullYear() + " time: "
			+ c_date.getHours() + ":"
			+ c_date.getMinutes() + ":"
			+ c_date.getSeconds();

		return error_time;
	}

	stackTrace (error) {
		try {
			let stacktrace = {
				category: error.category,
				stack: error.stack,
				name: error.name,
				msg: error.message,
				file: (typeof error.sourceURL !== "undefined")? error.sourceURL: error.fileName,
				lineNumber: (typeof error.line !== "undefined")? error.line : error.lineNumber,
				number: error.number,
				description: (typeof error.description !== 'undefined')? error.description: error.toString(),
				location: document.location.href
			};

			return stacktrace;

		} catch (err) {

			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);

			return error;
		}

	}
}

let ErrorLogger = new ErrorLoggerSingleton();

export default ErrorLogger;
