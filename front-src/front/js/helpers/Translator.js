/**
 * Created by Tomasz Dziuba on 04.01.2017.
 */
import i18next from 'i18next';

let lang = $('html').attr('lang');

function getTranslationKeys () {
	let keys;

	switch (lang.toLowerCase()) {
		case 'se':
			keys = require('../../../../web/locales/se_translation-keys.json');
			break;
		case 'fr':
			keys = require('../../../../web/locales/fr_translation-keys.json');
			break;
		default:
			keys = require('../../../../web/locales/en_translation-keys.json');
	}

	return keys;
}

export default class Translator {

	options = {};
	keys = getTranslationKeys();
	translator;

	constructor(options) {
		let self = this;

		this.options = $.extend({}, {
			pattern: /TK_([a-zA-Z_]+)/gm,
			selector: '.translation',
			language: lang
		}, options);

		i18next.init({
			lng: lang,
			resources: {
				se: {
					translation: self.keys
				},
				fr: {
					translation: self.keys
				}
			}
		}, () => {
			self.setTranslator( i18next );
		});

	}

	setTranslator (translator) {
		this.translator = translator;
	}

	parse() {
		let self = this,
			selector = $(this.options.selector);

		$.each(selector, function () {
			self.replaceText($(this));
		});
	}

	replaceText(element, t) {
		let text = (typeof t !== 'undefined') ? t : element.text().trim(),
			newText = this.translator.t(text);

		element.text(newText);
	}

	translate (key) {
		return this.translator.t(key);
	}
}
