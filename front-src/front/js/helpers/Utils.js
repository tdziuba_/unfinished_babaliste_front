/**
 * Created by Tomasz Dziuba on 13.02.2017.
 */

/**
 * check if element in on screen and can be visible
 * see http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport#answer-16270434
 *
 * @param element
 * @returns {boolean}
 */
export function isElementOnScreen (element) {
	let rect, el;

	if (typeof jQuery === "function" && element instanceof jQuery) {
		el = element[0];
	} else {
		el = element;
	}

	rect = el.getBoundingClientRect();

	return ( rect.bottom > 0 && rect.right > 0 && rect.left < $(window).width() && rect.top < $(window).height() );
}

/**
 * check if using Storage is possible
 * @param {string} storageType
 * @returns {boolean}
 */
export function hasStorage (storageType) {
	let testName = 'has_' + storageType.toString();

	try {

		switch (storageType.toLowerCase().replace('storage', '')) {
			case 'local':
				localStorage.setItem(testName, 1);
				localStorage.removeItem(testName);

				return true;

			case 'session':
				sessionStorage.setItem(testName, 1);
				sessionStorage.removeItem(testName);

				return true;

			default:
				sessionStorage.setItem(testName, 1);
				sessionStorage.removeItem(testName);

				return true;
		}

	} catch (err) {
		return false;
	}
}

function getExpiresValue (days) {
	let date = new Date(),
		milisecondsMultiplier = (24*60*60*1000);

	date.setTime(date.getTime() + ( days * milisecondsMultiplier));

	return '; expires=' + date.toUTCString();
}

export function setCookie (name, value, days, path) {
	let p = (path) ? path : '/';
	let exp = (!days) ? '' : getExpiresValue(days);

	document.cookie = name + "=" + value + exp + "; path=" + p;
}

export function getCookie (name) {
	let match = document.cookie.match(new RegExp(name + '=([^;]+)'));

	if (match) return match[1];
}

export function deleteCookie (name) {
	document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
