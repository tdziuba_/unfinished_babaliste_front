/**
 * Created by Tomasz Dziuba on 28.12.2016.
 */
var path = require('path'),
	glob = require('glob'),
	webpack = require('webpack'),
	// ProgressBarPlugin = require('progress-bar-webpack-plugin'),
	ExtractTextPlugin = require("extract-text-webpack-plugin"),
	CompressionPlugin = require("compression-webpack-plugin"),
	PurifyCSSPlugin = require('purifycss-webpack'),
	sassLintPlugin = require('sasslint-webpack-plugin'),
	sassLoaderPaths = [path.resolve(__dirname, "./front/scss"), path.resolve(__dirname, "./node_modules/bootstrap/scss")];

module.exports = {
	entry: {
		feed: ['./front/js/feed.js'],
		details: ['./front/js/details.js'],
		'customer-service': ['./front/js/customer-service.js'],
		profile: ['./front/js/profile.js'],
		grid: [
			'./front/js/vendor/masonry.pkgd.js',
			'./front/js/vendor/imagesloaded.pkgd.js',
			'./front/js/components/feed/LoadMore.js',
			'./front/js/components/feed/GridLayout.js'
		],
		vendor: [
			'./front/js/vendor/jquery.slim.js',
			'tether',
			'bootstrap',
			'./front/js/vendor/hammer.js',
			'./front/js/vendor/hammer-time.min.js'
		]
	},
	output: {
		path: path.join(__dirname, '../web'),
		filename: 'js/cbApp.[name].js',
		chunkFilename: 'js/cbApp.[name]-[id].chunk.js',
		publicPath: '/',
		library: ["cbApp", "[name]"],
		libraryTarget: "this"
	},

	externals: {
		jQuery: 'jQuery',
		Tether: 'Tether',
		cbApp: 'cbApp'
	},

	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			'Tether': 'tether',
			'window.Tether': 'tether'
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false
		}),
		new webpack.optimize.UglifyJsPlugin({
			beautify: false,
			mangle: {
				screw_ie8: true,
				keep_fnames: true
			},
			compress: {
				screw_ie8: true
			},
			comments: false
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'commons',
			children: true,
			minChunks: Infinity,
			async: true,
			chunks: ['vendor', './front/js/common.js']
		}),
		new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 5 }),
		new webpack.optimize.MinChunkSizePlugin({ minChunkSize: 100 }),
		new webpack.optimize.AggressiveMergingPlugin({
			minSizeReduce: 1.5,
			moveToParents: true
		}),
		new ExtractTextPlugin({ filename: 'css/[name].css', disable: false, allChunks: true }),

		// new PurifyCSSPlugin({
		// 	verbose: true,
		// 	minimize: true,
		// 	paths: glob.sync(`${PATHS}/*`),
		// 	styleExtensions: ['.css']
		// }),
		new CompressionPlugin({
			asset: "[path].gz[query]",
			algorithm: "gzip",
			test: /\.js$|\.css$/,
			threshold: 10240,
			minRatio: 0.8
		})
	],
	resolve: {
		extensions: [".webpack.js", ".js", ".scss"],
		modules: [
			path.join(__dirname, "front"),
			"node_modules"
		],
		alias: {
			Handlebars: 'handlebars/dist/handlebars.min.js',
			'handlebars' : 'handlebars/dist/handlebars.js',
			'Tether': 'tether/dist/js/tether.js'
		}
	},
	module: {

		rules: [
			{
				test: /\.js$/,
				enforce: "pre",
				loader: "eslint-loader",
				exclude: /node_modules/
			},
			{
				test: /\.js$/,
				use: 'babel-loader',
				exclude: /node_modules/
			},

			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use: [
						{
							loader: "css-loader",
							options: {
								sourceMap: true
							}
						},
						{
							loader: "sass-loader",
							options: {
								sourceMap: true,
								includePaths: sassLoaderPaths
							}
						}
					],

				})
			},

			// fonts
			{ test: /\.woff$/, use: "url-loader?prefix=font/&limit=5000&mimetype=application/font-woff" },
			{ test: /\.ttf$/,  use: "file-loader?prefix=font/" },
			{ test: /\.eot$/,  use: "file-loader?prefix=font/" },
			{ test: /\.svg$/,  use: "file-loader?prefix=font/" },

			//img
			{ test: /\.(png|gif|jpg|svg)$/, use: 'url-loader?name=/web/[path][name].[ext]' }
		]

		// preLoaders: [
		// 	//{ test: /\.js?$/, loader: 'eslint-loader', exclude: /node_modules/ },
		// 	{ test: /\.js$/, loader: "source-map-loader" },
		// ],
		// loaders: [
		// 	{ test: /\.js?$/, loader: 'babel-loader', exclude: /node_modules/ },
		// 	{ test: /\.scss$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract("style-loader", "css?sourceMap&-url!sass-loader?sourceMap") },
		// 	{ test: /\.json$/, loader: "json-loader" },
		//
		// 	{ test: /\.woff$/, loader: "url-loader?prefix=font/&limit=5000&mimetype=application/font-woff" },
		// 	{ test: /\.ttf$/,  loader: "file-loader?prefix=font/" },
		// 	{ test: /\.eot$/,  loader: "file-loader?prefix=font/" },
		// 	{ test: /\.svg$/,  loader: "file-loader?prefix=font/" },
		//
		// 	//img
		// 	{ test: /\.(png|gif|jpg)$/, loader: 'url-loader?name=[path][name].[ext]' }
		// ]
	}

}
