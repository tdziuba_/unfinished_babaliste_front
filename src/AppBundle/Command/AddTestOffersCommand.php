<?php

namespace AppBundle\Command;

use AppBundle\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class AddTestOffersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('cb:add_test_offers')
            ->setDescription('Creates test offers')
            ->addArgument('count', InputArgument::REQUIRED, 'Who do you want to greet?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $source_em = $this->getContainer()->get('doctrine')->getManager('scraped');
        $target_em = $this->getContainer()->get('doctrine')->getManager('default');

        $limit = $input->getArgument('count');

        $counter = 0;

        if (is_numeric($limit) && $limit > 0) {

            $sql = "SELECT * FROM offer_item WHERE imported_at is NULL and details_updated_at is not null and list_picture != '' LIMIT $limit";

            $stmt = $source_em->getConnection()->prepare($sql);
            $stmt->execute();

            $results = $stmt->fetchAll();

            foreach ($results as $row) {

                $offer = new Offer();

                $offer->setTitle($row['details_title']);
                $offer->setPrice((int)$row['details_price']);
                $offer->setDescription($row['details_description']);
                $offer->setLocationName($row['details_location']);
                $offer->setCategoryName($row['details_category']);
                $offer->setOwnerName($row['details_owner_name']);
                $offer->setOwnerAvatar($row['details_avatar_url']);

                $ch = curl_init($row['list_picture']);
                curl_setopt($ch, CURLOPT_NOBODY, true);
                curl_exec($ch);
                $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);

                if ($retcode == 200) {
                    list($width, $height, $type, $attr) = getimagesize($row['list_picture']);
                    $offer->setFirstPictureRatio($width.'x'.$height);
                }

                if ($row['details_pictures'] != '') {
                    $pictures = $row['details_pictures'];
                } else {
                    $pictures = $row['list_picture'];
                }

                $offer->setItemPictures($pictures);

                $sql = "UPDATE offer_item
            SET imported_at = 'NOW()'
            WHERE id = " . $row['id'];

                $stmt = $source_em->getConnection()->prepare($sql);
                $stmt->execute();
                $source_em->flush();

                if ($retcode == 200) {
                    $target_em->merge($offer);
                    $target_em->flush();
                    $counter++;
                    $output->write($row['list_title'], true);
                } else {
                    $offer = null;
                }

            }

            $output->write('Created offers: '.$counter, true);

        } else {

            $output->write('Invalid "count" argument value (integer expected).', true);

        }



    }

}