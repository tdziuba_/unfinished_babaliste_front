<?php
/**
 * Created by PhpStorm.
 * User: Tomasz Dziuba
 * Date: 03.03.2017
 * Time: 15:19
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Translations\JsonFileDumper;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Finder\Finder;

class CreateTranslationForFrontendCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('cb:create_frontend_translation')
            ->setDescription('Creates translation keys in json format for il18n for frontend components usage')
            ->addArgument('lang', InputArgument::REQUIRED, 'Type language: (fr|se)')
            ->setHelp('This command allows you to create a json file with translations keys in format used by frontend')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lang = $input->getArgument('lang');
        $loader = new XliffFileLoader();
        $output->writeln([
            'Retriving keys...',
            '',
        ]);
        $find = new Finder();
        $find->files()->in('app/Resources/translations')->name('messages.' . $lang . '.xlf');

        foreach ($find as $file) {
            $catalogue = $loader->load($file , $lang);

            $dumper = new JsonFileDumper();
            $dumper->setRelativePathTemplate('%locale%_translation-keys.%extension%');
            $dumper->dump($catalogue, array(
                'path' => '%kernel.root_dir%/../web/locales/'
            ));

            $output->writeln([
                'Translation file created at: /web/locales/'.$lang.'_translation-keys.json',
                '============',
                '',
            ]);
        }


    }
}