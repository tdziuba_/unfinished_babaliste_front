<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Loader\JsonFileLoader;
use Mobile_Detect;

class BaseController extends Controller
{
	public $md;
	public $mobile_class = '';
	public $is_mobile;
	public $is_tablet;
	public $is_android;
	public $is_iOS;
	public $show_banner;
	public $translator;

    public function __construct()
	{

		// translations
		$request = Request::createFromGlobals();
		$locale = $request->getLocale();
		$this->translator = new Translator($locale, new MessageSelector());
		$this->translator->addLoader('json', new JsonFileLoader());

		$this->translator->addResource(
			'json',
			$locale.'_translation-keys.json',
			$locale,
			'navigation'
		);
	}

	public function getTemplateCommons ()
    {
        $service = $this->get('app.controller_commons');

        return $service->getTemplateCommons();
    }

}
