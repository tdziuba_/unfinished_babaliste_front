<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CustomerServiceController extends BaseController
{

	/**
	 * Matches /customer_service/*
	 *
	 * @Route("/customer-service/{section}",
	 *     defaults = { "section" = "contact" },
	 *     options = { "expose" = true },
	 *     name = "customer_service",
	 * )
	 *
	 * @param $section
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction($section)
    {

		return $this->render(':default:customer-service.html.twig',
			array_merge(
                [
                    'section' => $section,
                    'url' => 'customer-service/'.$section,

                ],
                $this->getTemplateCommons()
            )
		);
    }

    /**
     * Route for showing frontend translation keys - just for easier developing,
     * accessible only on dev environment
     *
     * @Route("/show-translation/{lang}",
     *     defaults = { "lang" = "fr" },
     *     options = { "expose" = false },
     *     name = "show-translation",
     * )
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function translationKeysAction($lang, Request $request)
	{
        $response = new Response();
        $lang = (!$lang) ? $request->getLocale() : $lang;
        $kernel = $this->get('kernel');

        if ($kernel->getEnvironment() == 'dev') {
            $web_path = $request->server->get('DOCUMENT_ROOT');
            $file = $web_path . '/locales/' . $lang . '_translation-keys.json';

            $response->setContent(file_get_contents($file));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            throw $this->createNotFoundException('The page does not exist');
        }

	}
}
