<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends BaseController
{
    /**
     * @Route("/", name="homepage")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 32);
		$searched_text = $request->get('inp_text', '');

        $em = $this->getDoctrine()->getManager();

        $offers = $em->createQueryBuilder()
            ->select('o')
            ->from('AppBundle:Offer', 'o')
            ->orderBy('o.id', 'DESC')
            ->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getResult();

        if ($request->isXmlHttpRequest()) {

            $offers_data = array();

			// TODO: dorobić ratio dla obrazków

            foreach ($offers as $single_offer) {
                $offers_data[] = array(
                    'id' => $single_offer->getId(),
                    'title' => $single_offer->getTitle(),
                    'price' => $single_offer->getPrice(),
                    'first_picture' => $single_offer->getFirstPictureUrl(),
                    'description' => $single_offer->getDescription(),
					'location_name' => $single_offer->getLocationName(),
					'ratio' => $single_offer->getFirstPictureRatioRate() // ta metoda nic na razie nie zwraca
                );
            }

            // TODO: dorobić obliczanie maksymalnej ilości ogłoszeń - totalCount

            $response = new JsonResponse();
            $response->setData(array(
                'offers' => $offers_data,
				'status' => 'success'
            ));
            return $response;

        } else {
            return $this->render('default/index.html.twig',
                array_merge(
                    array(
                        'offers' => $offers,
                        'showSelectedWrapper' => ($searched_text !== ''), // TODO: dorobić sprawdzanie czy są wybrana kategoria i wybrana lokacja
                    ),
                    $this->getTemplateCommons()
                )
            );
        }

    }

    /**
	 * Matches /show/*
	 *
	 * @Route("/show/{offer_id}",
	 *     defaults = { "offer_id" = 1 },
	 *     options = { "expose" = true },
	 *     name = "show",
	 * )
	 *
	 * @param int $offer_id offer id
	 * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($offer_id) {

        $em = $this->getDoctrine()->getManager();

        $offer = $em->getRepository('AppBundle:Offer')->find($offer_id);

        if (!$offer) {
            throw $this->createNotFoundException(
                'No product found for id '.$offer_id
            );
        }

        return $this->render('default/show.html.twig',
            array_merge(
                array(
                    'offer' => $offer,
                ),
                $this->getTemplateCommons()
            )
        );


    }

}
