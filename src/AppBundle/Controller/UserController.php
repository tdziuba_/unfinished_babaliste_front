<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class UserController extends BaseController
{

	/**
	 * Matches /user/:id
	 *
	 * @Route("/user/{id}",
	 *     requirements={"id" = "\d+"},
	 *     defaults={"id" = 1},
	 *     options = { "expose" = true },
	 *     name = "user",
	 * )
	 *
	 * @param int $id user id
	 * @return void
	 */
    public function indexAction($id)
    {
		$em = $this->getDoctrine()->getManager();

		$offers = $em->createQueryBuilder()
			->select('o')
			->from('AppBundle:Offer', 'o')
			->orderBy('o.id', 'DESC')
			->getQuery()
			->setFirstResult(61)
			->setMaxResults(7)
			->getResult();

		return $this->render(':default:profile.html.twig', [
			'offers' => $offers,
			'mobileClass' => $this->mobile_class,
			'isMobile' => $this->is_mobile,
			'isTablet' => $this->is_tablet,
			'isAndroid' => $this->is_android,
			'isiOS' => $this->is_iOS,
			'showBanner' => $this->show_banner
		]);
    }

    /**
	 * Matches /user/:id/sold-offers
	 *
	 * @Route("/user/{id}/sold-offers",
	 *     requirements={"id" = "\d+"},
	 *     defaults={"id" = 1},
	 *     options = { "expose" = true },
	 *     name = "user-sold-offers",
	 * )
	 *
	 * @param int $id user id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function soldOffersAction($id)
	{
		$soldOffers = [];

		return $this->render(':default:profile.html.twig',
            array_merge(
                [
			        'soldOffers' => $soldOffers,
                ],
			    $this->getTemplateCommons()
            )
		);
	}


}
