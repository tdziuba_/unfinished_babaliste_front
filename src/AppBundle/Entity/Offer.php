<?php
/**
 * Created by PhpStorm.
 * User: mmalyszko
 * Date: 03.02.2017
 * Time: 12:09
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Offer
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    private $first_picture_ratio;

    /**
     * @param mixed $first_picture_ratio
     */
    public function setFirstPictureRatio($first_picture_ratio)
    {
        $this->first_picture_ratio = $first_picture_ratio;
    }

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     */
    private $location_name;

    /**
     * @ORM\Column(type="string")
     */
    private $category_name;

    /**
     * @ORM\Column(type="string")
     */
    private $owner_name;

    /**
     * @ORM\Column(type="text")
     */
    private $item_pictures;

    /**
     * @ORM\Column(type="string")
     */
    private $owner_avatar;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Offer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Offer
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Offer
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Offer
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocationName()
    {
        return $this->location_name;
    }

    /**
     * @param mixed $location_name
     * @return Offer
     */
    public function setLocationName($location_name)
    {
        $this->location_name = $location_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    /**
     * @param mixed $category_name
     * @return Offer
     */
    public function setCategoryName($category_name)
    {
        $this->category_name = $category_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwnerName()
    {
        return $this->owner_name;
    }

    /**
     * @param mixed $owner_name
     * @return Offer
     */
    public function setOwnerName($owner_name)
    {
        $this->owner_name = $owner_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getItemPictures()
    {
        return $this->item_pictures;
    }

    public function getFirstPictureUrl()
    {
        $pictures = explode( ',', $this->item_pictures);
        return (isset($pictures[0])) ? $pictures[0] : false;
    }

    public function getPicturesUrls()
    {
        return explode( ',', $this->item_pictures);
    }

	public function getFirstPictureRatioRate()
	{
        $size = explode( 'x', $this->first_picture_ratio);

		return round(($size[0] / $size[1]), 2);
	}

    /**
     * @param mixed $item_pictures
     * @return Offer
     */
    public function setItemPictures($item_pictures)
    {
        $this->item_pictures = $item_pictures;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwnerAvatar()
    {
        return $this->owner_avatar;
    }

    /**
     * @param mixed $owner_avatar
     * @return Offer
     */
    public function setOwnerAvatar($owner_avatar)
    {
        $this->owner_avatar = $owner_avatar;
        return $this;
    }


}