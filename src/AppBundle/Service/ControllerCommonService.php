<?php

/**
 * Created by PhpStorm.
 * User: Tomasz Dziuba
 * Date: 03.03.2017
 * Time: 13:25
 */
namespace AppBundle\Service;

use \Mobile_Detect;

class ControllerCommonService
{
    public function __construct()
    {
        $this->md = new Mobile_Detect();
        
    }
    
    public function getTemplateCommons() {
        $mobileClass = ($this->md->isMobile()) ? ' isMobile' : '';
        $mobileClass .= ($this->md->isAndroidOS()) ? ' android' : '';
        $mobileClass .= ($this->md->isiOS()) ? ' iOS' : '';

        $is_mobile = ($this->md->isMobile() && !$this->md->isTablet());
        $is_tablet = $this->md->isTablet();
        $is_android = $this->md->isAndroidOS();
        $is_iOS = $this->md->isiOS();
        $show_banner = !isset( $_COOKIE['hideMobileAppBanner'] );

        return array(
            'mobileClass' => $mobileClass,
            'isMobile' => $is_mobile,
            'isTablet' => $is_tablet,
            'isAndroid' => $is_android,
            'isiOS' => $is_iOS,
            'showBanner' => $show_banner,
            'showSendMeAppConfirm' => isset( $_COOKIE['hideSendMeAppForm'] )
        );
    }
}