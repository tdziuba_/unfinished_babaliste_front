<?php

/**
 * Created by PhpStorm.
 * User: Tomasz Dziuba
 * Date: 03.03.2017
 * Time: 15:06
 */
namespace AppBundle\Translations;

use Symfony\Component\Translation\MessageCatalogue;
use Symfony\Component\Translation\Dumper\FileDumper;

class JsonFileDumper extends FileDumper
{

    public function formatCatalogue(MessageCatalogue $messages, $domain = null, array $options = array())
    {
        $output = [];

        foreach ($messages->all($domain) as $source => $target) {
            $replace = trim(strtolower($source));
            $replace = preg_replace( "/[^a-z0-9.]+/i", "_", $replace );
            $output[$replace] = $target;
        }

        return json_encode($output);
    }



    protected function getExtension()
    {
        return 'json';
    }
}
